from conn import db
import csv
from datetime import datetime
import time

today = datetime.today().strftime('%Y-%m-%d')

# set flow id after insert
flow_id = 123

def none_to_null(val):
    if val is None:
        return 'null'
    else:
        return f"'{val}'"

def get_tony_query(row, flow_id, closed_on, group_id = 1):
    query = (f"INSERT INTO tony.subscription(reporting_date, money_farm_subscription_id, ndg, subscription_start_on_year, subscription_start_on_month, subscription_start_on_day, subscription_closed_on_year, "
             f"subscription_closed_on_month, subscription_closed_on_day, aui_report_type, ade_account_type, account_version, tax_treatment_type, tax_regime, financial_leverage, income_tax_treatment_type, "
             f"account_statement_reporting_frequency, created_on, skip_ade_reporting, investment_line, account_type, flow_id, recovery_period_id, money_farm_discretionary_line_id, record_type, benchmark, "
             f"benchmark_year, benchmark_month, benchmark_day, ignored, closure_request_year, closure_request_month, closure_request_day, partner_id, partner_user_id, is_last_discretionary_for_user, initial_report, group_id)\n"
             f"VALUES ( {today}, '{row[2]}', '{row[3]}', '{row[4]}','{row[5]}','{row[6]}', '{closed_on.year}', '{closed_on.month}', '{closed_on.day}', '{row[10]}', '{row[11]}', '{row[12]}', '{row[13]}', '{row[14]}', '{row[15]}', "
             f"'{row[16]}', '{row[17]}', now(), {none_to_null(row[19])}, '{row[20]}', '{row[21]}', {flow_id}, null, {none_to_null(row[24])}, '{row[25]}', {none_to_null(row[26])}, {none_to_null(row[27])}, "
             f"{none_to_null(row[28])}, {none_to_null(row[29])}, 0, {none_to_null(row[31])}, {none_to_null(row[32])}, {none_to_null(row[33])}, '{row[34]}', {none_to_null(row[35])}, {none_to_null(row[36])}, {none_to_null(row[37])}, {group_id}); \n")
                                                # ^           ^
    return query
def write_data(row, cursor, output_file, log):
    subscription_id = row[0]
    disc_status_updated, tony_updated = False, False
    print(f"Subscription {subscription_id}:")
    cursor.execute(f"select count(*) from mf_lisa_rto.mf_portfolio "
                   f"where subscription_id = {subscription_id} "
                   f"and parent_id is not null and closed_on is null")
    open_ct = cursor.fetchone()[0]
    print(f"  has {open_ct} open portfolios")

    last_closed = None
    if open_ct == 0:
        cursor.execute(f"select max(closed_on) from mf_lisa_rto.mf_portfolio "
                       f"where subscription_id = {subscription_id} "
                       f"and parent_id is not null")
        last_closed = cursor.fetchone()[0]
        print(f"    last closed on {last_closed}")


    cursor.execute(f"select status from mf_lisa_rto.mf_user_discretionary_status where subscription_id = {subscription_id}")
    status = cursor.fetchone()[0]
    print(f"  has discretionary status {status}")

    if open_ct == 0 and status == 'open':
        output_file.write(f"-- Sub {subscription_id} discretionary status {status} but last portfolio closed on {last_closed}. Fix \n")
        disc_update_query = f"update mf_lisa_rto.mf_user_discretionary_status set status = 'closed', updated_at = '{last_closed}' where subscription_id = {subscription_id}; \n"
        output_file.write(disc_update_query)
        disc_status_updated = True

    cursor.execute(f"select * from tony.subscription where money_farm_subscription_id = '{subscription_id}_DISC' "
                   f"and account_type = 'P0' and ignored <> 1 and flow_id is not null "
                   f"order by reporting_date desc limit 1")
    p0_record = cursor.fetchone()
    p0_is_closed = p0_record[7] is not None
    if not p0_is_closed and open_ct == 0:
        print(f"  sub {subscription_id} is open on tony")
        output_file.write(f"-- Sub {subscription_id} insert tony P0 closure for {subscription_id} \n")
        output_file.write(get_tony_query(p0_record, flow_id, last_closed))
        tony_updated = True

    action = row[8]
    note = row[7]
    if ((action == 'lisa+tony' and not (tony_updated and disc_status_updated))
            or (action == 'nothing' and (tony_updated or disc_status_updated))
            or (action == 'lisa' and tony_updated)
            or (action == 'tony' and disc_status_updated)):
        log.write(f"Sub {subscription_id}: \n")
        log.write(f"Note: {note} \n")
        log.write(f"  open portfolios: {open_ct} \n")
        log.write(f"  discretionary status: {status} \n")
        log.write(f"  discretionary status updated: {disc_status_updated} \n")
        log.write(f"  tony record: id {p0_record[0]}, account_id: {p0_record[2]} closure year: {p0_record[7]} \n")
        log.write(f"  tony updated: {tony_updated} \n")
        log.write(f"--- \n")


cursor = db.cursor()

with open('outstanding.log', 'w') as log:
    with open('fix_query.sql', 'w') as output:
        # with open('files/test.csv', 'r') as csvfile:
        with open('files/all_data.csv', 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=';')
            header = next(reader) # header
            for row in reader:
                write_data(row, cursor, output, log)
                time.sleep(0.5)


cursor.close()
db.close()